package googleDocsBlog

import java.util.*

object PropertiesReader {
    private var properties: Properties? = null

    @Synchronized fun read(): Properties? {
        if (properties != null) {
            return properties
        }

        properties = Properties()
        val settingsStream = this::class.java.getResourceAsStream("/settings.properties")
        properties?.load(settingsStream)
        return properties
    }
}